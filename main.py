#Este arquivo implementa o cliente do terceiro passo da Prova Desenvolvedor Web

import requests
import json

response = requests.get('http://localhost:5000/todo/api/v1.0/detectCollisions')
collisionsList = []

#preenche com uma lista de tuplas a collisionsList, de acordo com o json que foi recebido
#atraves da url acima
data = response.json()
for e in data["detectCollisions"]:
	tmp = e["description"].split(" ")
	collisionsList.append((int(tmp[0]), int(tmp[1])))


#detecta se dois nos pertecem a mesma rede de colisao
def detectCollision(id1, id2): 
	collisionNetworks = []
	temp = []

	#itera na lista formada por todos os nos listados no arquivo de colisao
	for t1 in collisionsList:
		for t2 in collisionsList:
			#se os nos sao diferentes e tem pelo menos um elemento em comum
			if t1 != t2 and (t1[0] in t2 or t1[1] in t2):
				#se nenhum dos elementos de ambos os nos pertence a temp, e hora de iniciar uma
				#nova rede de colisao, por isso temp e esvaziado
				if not (t1[0] in temp or t1[1] in temp or t2[0] in temp or t2[1] in temp):
					temp = []
				if t1[0] not in temp:
					temp.append(t1[0])
				if t1[1] not in temp:
					temp.append(t1[1])
				if t2[0] not in temp:
					temp.append(t2[0])
				if t2[1] not in temp:
					temp.append(t2[1])
				#sempre que uma nova lista de colisao e criada, ela deve ser adicionada
				#como uma sublista da lista de colisoes
				if temp not in collisionNetworks:
					collisionNetworks.append(temp)

	#itera pelas listas de colisao que sao subslistas da lista collisionNetworks
	for l in collisionNetworks:
		#caso os dois ids buscados pertencam a mesma sublista, e porque fazem parte da 
		#mesma rede de colisao, e por isso a funcao retorna true
		if id1 in l and id2 in l:
			return True
	return False


print detectCollision(1, 3)