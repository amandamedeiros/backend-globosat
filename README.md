O arquivo collision-detection.py contem apenas o primeiro e o segundo passo, e funciona utilizando o arquivo de texto collisions.txt.

Os arquivos app.py, tutorial.py e main.py são responsáveis pelo terceiro passo.
Para rodar o terceiro passo, basta iniciar o web service através dos comandos: 

chmod a+x app.py
python tutorial.py

Uma vez iniciado, basta rodar o arquivo main.py, e o resultado será printado no terminal.

OBS. É preciso ter instalado o flask e o request.