#!/usr/bin/env python
import web

urls = (
    '/', 'index'
)

class index:
    def GET(self):
        content = ' {{"node": "1 2","action": "addCollision"}, { "node": "2 3","action": "addCollision"},{ "node": "1 4",  "action": "addCollision"}, { "node": "5 6", "action": "addCollision"} ,{ "node": "6 7","action": "addCollision"}} '
        return content

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
