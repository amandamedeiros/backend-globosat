#Este arquivo implementa o restful web service do terceiro passo da Prova Desenvolvedor Web

#!flask/bin/python
from flask import Flask, jsonify
from flask import request
from flask import abort

app = Flask(__name__)

#Conteudo a ser acessado, com os nos de colisao
detectCollisions = [
    {
        'id': 1,
        'description': u'1 2', 
    },
    {
        'id': 2,
        'description': u'2 3', 
    },
    {
        'id': 3,
        'description': u'1 4', 
    },
    {
        'id': 4,
        'description': u'5 6', 
    },
    {
        'id': 5,
        'description': u'6 7', 
    }
]

#Nos de colisao a serem adicionados
addCollisions = [
    {
        'id': 6,
        'description': '5 8', 
    },
    {
        'id': 7,
        'description': '1 9', 
    }
]

#Rota a ser acessada para obter, no cliente, os nos de colisao
@app.route('/todo/api/v1.0/detectCollisions', methods=['GET'])
def get_detectCollisions():
    return jsonify({'detectCollisions': detectCollisions})

@app.route('/todo/api/v1.0/addCollisions', methods=['POST'])
def create_collisions():
    if not request.json or not 'description' in request.json:
       abort(400)
    collisions = {
        'id': 0,
        'description': request.json.get('description', ""),
    }
    addCollisions.append(collisions)
    return jsonify({'collisions': collisions}), 201
   #requests.put("http://httpbin.org/put", data = {"key":"value"})


if __name__ == '__main__':
    app.run(debug=True)